# SoPraPflichtenheft

## Nützliche Befehle
### Sequenz Diagramme automatisch rendern
benötigt:
  - `inotify-tools` package
  - http://sdedit.sourceforge.net/

```
file=<file>
output=<output>
$ while inotifywait -e close_write <file>; do java -jar sdedit-4.2.1.jar -o <output> -t pdf  <file> -r Landscape; done
```

### Dia exportieren via der Kommandozeile
`$ dia figures/useCaseDiagram/<file>.dia --export figures/useCaseDiagram/<output>.tex -t tex`

### Rekursiv Zeichen in Dateinamen ändern
Beispiel: alle Unterstriche entfernen in Dateinamen die auf `.pdf` enden im Verzeichnis `figures/gui`
`$ find figures/gui -type f -name "*.pdf" -execdir perl-rename -v 's/ß/ss/g' '{}' \;`

### includepdf für alle pdfs im Verzeichnis ausgeben
`$ find . -iname "*.pdf" -exec echo \\includepdf{'{}'}`